# Bug Bounty Writeups

List of bug bounty writeups

SSRF 
-------------------

https://medium.com/bugbountywriteup/leaking-aws-metadata-f5bc8de03284 [AWS SSRF] 



Race Condition
-------------------

- https://medium.com/@pravinponnusamy/race-condition-vulnerability-found-in-bug-bounty-program-573260454c43
- https://medium.com/bugbountywriteup/upload-to-the-future-1fd38fd502bd 



Cache Poisoning
-------------------

- https://medium.com/bugbountywriteup/fun-with-header-and-forget-password-with-a-twist-af095b426fb2 [using X-Forwarded-Host]
- https://iustin24.github.io/Cache-Key-Normalization-Denial-of-Service/?cb=1 



XXE
-------------------

- https://blog.netspi.com/playing-content-type-xxe-json-endpoints/ [Converting JSON to XML]
- https://medium.com/@estebancano/unique-xxe-to-aws-keys-journey-afe678989b2b [Unique way to identify ]



Weird Logic
-------------------

https://omespino.com/write-up-google-bug-bounty-xss-to-cloud-shell-instance-takeover-rce-as-root-5000-usd/



Finding Cloudfare Origin IP Server
--------------------------------------

https://blog.detectify.com/2019/07/31/bypassing-cloudflare-waf-with-the-origin-server-ip-address/



Android Pentest
-------------------

- https://www.hackingarticles.in/android-penetration-testing-apk-reverse-engineering/ 
- https://www.hackingarticles.in/android-penetration-testing-apk-reversing-part-2/ 
- https://www.hackingarticles.in/android-pentest-deep-link-exploitation/ 
- https://www.hackingarticles.in/android-penetration-testing-webview-attacks/ 
- https://www.hackingarticles.in/android-penetration-testing-frida/ 
- https://www.hackingarticles.in/android-pentest-lab-setup-adb-command-cheatsheet/ 
- https://www.hackingarticles.in/android-hooking-and-sslpinning-using-objection-framework/ 
- https://www.hackingarticles.in/android-penetration-testing-drozer/ 
- https://www.hackingarticles.in/android-pentest-automated-analysis-using-mobsf/ 



GitHub Recon
-------------------

https://orwaatyat.medium.com/your-full-map-to-github-recon-and-leaks-exposure-860c37ca2c82 



Password Reset Flaws
---------------------

https://anugrahsr.github.io/posts/10-Password-reset-flaws/ 
